package com.rizkasalma.bangunruang;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edit_alas, edit_tinggi_segitiga, edit_tinggi;
    Button button_calculate;
    TextView text_result;
    private static final String STATE_RESULT = "state_result";
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_RESULT, text_result.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_alas = findViewById(R.id.edit_alas);
        edit_tinggi_segitiga = findViewById(R.id.edit_tinggi_segitiga);
        edit_tinggi = findViewById(R.id.edit_tinggi);
        button_calculate = findViewById(R.id.button_calculate);
        text_result = findViewById(R.id.text_result);

        button_calculate.setOnClickListener(this);
        if (savedInstanceState != null) {
            String result = savedInstanceState.getString(STATE_RESULT);
            text_result.setText(result);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.button_calculate){
            String inputAlas = edit_alas.getText().toString().trim();
            String inputTinggiS = edit_tinggi_segitiga.getText().toString().trim();
            String inputTinggi = edit_tinggi.getText().toString().trim();

            boolean isEmptyFields = false;
            boolean isInvalidDouble = false;

            if(TextUtils.isEmpty(inputAlas)){
                isEmptyFields = true;
                edit_alas.setError("Tolong Field ini diisi yaa");
            }
            if(TextUtils.isEmpty(inputTinggiS)){
                isEmptyFields = true;
                edit_tinggi_segitiga.setError("Tolong Field ini diisi yaa");
            }
            if(TextUtils.isEmpty(inputTinggi)){
                isEmptyFields = true;
                edit_tinggi.setError("Tolong Field ini diisi yaa");
            }


            Double alas = toDouble(inputAlas);
            Double tinggis = toDouble(inputTinggiS);
            Double tinggi = toDouble(inputTinggi);

            if(alas==null){
                isInvalidDouble = true;
                edit_alas.setError("Tolong Field ini diisi dg valid yaaa");
            }

            if(tinggis==null){
                isInvalidDouble = true;
                edit_tinggi_segitiga.setError("Tolong Field ini diisi dg valid yaaa");
            }

            if(tinggi==null){
                isInvalidDouble = true;
                edit_tinggi.setError("Tolong Field ini diisi dg valid yaaa");
            }
            if(!isInvalidDouble && !isEmptyFields){
                double volume = (alas * tinggis)/2 * tinggi;
                text_result.setText(String.valueOf(volume));
            }
        }

    }

    Double toDouble(String str) {
        try {
            return Double.valueOf(str);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
